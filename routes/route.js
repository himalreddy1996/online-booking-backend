const router = require("express").Router();
const json_data = require("../json/qa-index.json");

router.route("/packages").get((req, res) => {
	res.send(json_data.packages);
});
router.route("/packages/:packageId").get((req, res) => {
	let packageDetails = json_data.packages.find(eachPackage => eachPackage.id == req.params.packageId);
	res.send(packageDetails);
});

router.route("/register").post((req, res) => {
	res.send("registeted");
});
router.route("/login").post((req, res) => {
	var result = {};
	result["userDetails"] = {};
	if(req.body.userName == "" ||  req.body.password == ""){
		result["status"] = "failure";
		result["msg"]= "invalid username or password";
	}else{
		let usersData = json_data.users;
		let userExistsData = usersData.find(eachUser => eachUser.username == req.body.userName)
		if(typeof userExistsData == "undefined" || userExistsData.password != req.body.password){
			result["status"] = "failure";
			result["msg"]= "invalid username or password";
		}else{
			result["status"] = "success";
			result["msg"]= "login success";
			result["userDetails"]["id"] = userExistsData.id;
			result["userDetails"]["name"] = userExistsData.name;
			result["userDetails"]["userName"] = userExistsData.username;
			result["userDetails"]["phoneNo"] = userExistsData.phoneno;
			result["userDetails"]["emailId"] = userExistsData.emailid;
		}
	}
	res.send(result);
});
router.route("/payment").post((req, res) => {
	json_data.payments.push(req.body);
	res.send("success");
});
router.route("/payments").get((req, res) => {
	res.send(json_data.payments);
});
module.exports = router;
